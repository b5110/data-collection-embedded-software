/* Author: Ahmet Ozbay
 * Date: 01.04.2022
 * Purpose: Akıllı bileklik makine öğrenmesi algoritmasında veri seti oluşturması için 
 * MPU6050'den alınan verilerin işlenmiş ve işlenmemiş hallerini, MQTT üzerinden iletmek.
 */
 
#include "src/mpu6050_data.h"
#include "src/mqtt.h"

/* MQTT, mpu6050, mpu6050 verilerinin filtrelenmesi ve MQTT üzerinden iletilmesi için
 * gerekli "class" ve "structure" deklerasyonu. 
*/
Adafruit_MPU6050 mpu;
Mpu6050_Data_T mpu6050_data;
sensors_event_t a, g, temp;
MQTT_Send_Data_T mqtt_tx_package;
WiFiClient espClient;
PubSubClient client(espClient);

/* Buton ile adım atılıp atılmadığı algoritmaya girdi olarak verilecektir. 
 * Butona basılması adım atılmış olduğu anlamına gelir.
*/
const int button_pin = 0;
volatile uint16_t step_ctr {0};
volatile bool exti_flag {0};
void handleInterrupt(void);
void setup(void) 
{
  Serial.begin(115200);
  pinMode(button_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(button_pin), handleInterrupt, FALLING);
  // Gerekli kurulumlar
  wifi_setup();
  mqtt_setup(&client);
  
  
  mpu_setup(&mpu);

  // MQTT haberleşmesinin başladığını karşı tarafa bilgilendirme.
  client.publish("d_col_begin", "start"); //Topic name

  /* Filtrelerin kullanımından sonra sonuçların ortalamasının alınması ve MQTT mesaj 
   * yollama iterasyon sayısını ve periyodunu belirler.yollama iterasyon sayısını ve 
   * periyodunu belirler. Bu assignment yapılmazsa "0"a bölüm sonucu mikrodenetleyici 
   * "Hard Fault"a düşer.
   */
  mpu6050_data.sample_size = FILTER_SAMPLE_SIZE;
}

void loop() 
{ 
  client.loop();
  
  
  static uint64_t filter_time_stamp;
  if((millis() - filter_time_stamp) >= MPU6050_FILTER_PERIODE)      // TODO: Bu yapı yerine timer interrupt kullanılabilir.
  {
    static uint16_t sample_ctr {0};
    sample_ctr++;
    //TODO: Filter functions
    
    mpu.getEvent(&a, &g, &temp);   // Ham mpu6050 verileri I2C üzerinden alınır. Bu veriler m/s2 birimi cinsine dönüştürülür.
    Offset_mpu6050(&a, &g);        // Sensörün üretiminden kaynaklanan eklenmesi gereken offset değerleri alınan verilere eklenir.
    Sum_Raw_Data(&mpu6050_data, a, g);     // m/s2 birimi cinsine dönüştürülmüş ham verileri toplar.
    Pitch_Roll_Calculate(&mpu6050_data, a, g);  // Complimentary filtresi kullanılarak "pitch" ve "roll" hesabı yapılır.
    Calculate_Pitch_Roll_Characteristics(&mpu6050_data);  // Hesaplanan "pitch" ve "roll" açılarının türevler toplamı ve integrali hesaplanır.
    

    Serial.print("Temperature: ");
    Serial.print(temp.temperature);
    Serial.println(" degC");

    if(sample_ctr >= mpu6050_data.sample_size)
    {
      Create_MQTT_TX_Package(&mqtt_tx_package, mpu6050_data, &exti_flag);   // MQTT üzerinden yollanacak structure doldurulur.
      Print_Mqtt_TX_Pack(mqtt_tx_package);         // MQTT paketi seri monitor üzerinden gözlemlenebilir.
      //Print_Raw_MQTT_TX_Pack(mqtt_tx_package);       // Anlamlandırılmamış MQTT paketi seri monitörden görülebilir.
      client.publish(data_topic, (const uint8_t *)mqtt_tx_package.tx_buf, sizeof(mqtt_tx_package.tx_buf));       // MQTT üzerinden iletim.
      Clear_Mpu_Data(&mpu6050_data);                            // MPU6050 verilerinin tutulduğu structure "sample_size" hariç sıfırlanır.
      sample_ctr = 0;       
    }
    
    filter_time_stamp = millis();
  }
}

ICACHE_RAM_ATTR void handleInterrupt() 
{
  step_ctr++;
  exti_flag = 1;
}
